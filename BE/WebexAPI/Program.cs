﻿using RestSharp;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options =>
{
    options.AddDefaultPolicy(
        policy =>
        {
            policy.AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
        });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors();
app.UseHttpsRedirection();




app.MapGet("/can/reuniones", () =>
{
    var client = new RestClient("https://webexapis.com");
    var request = new RestRequest("/v1/meetings");
    request.AddHeader("Authorization", "Bearer NThjMzcwYzktOWZkZS00NmZjLWE3YzUtOGFjNWU2NGZlNTAxMWJlYWYyMWYtZWI0_P0A1_dbc9bba3-627f-4c39-ab95-e643faf6bc86");
    var response = client.Execute(request);
    if (response is not null) return Results.Ok(response.Content);
    return Results.BadRequest();
});

app.MapGet("/can/reuniones/{id}", (string id) => {
    var client = new RestClient("https://webexapis.com");
    var request = new RestRequest("/v1/meetings/"+id);
    request.AddHeader("Authorization", "Bearer NThjMzcwYzktOWZkZS00NmZjLWE3YzUtOGFjNWU2NGZlNTAxMWJlYWYyMWYtZWI0_P0A1_dbc9bba3-627f-4c39-ab95-e643faf6bc86");
    var response = client.Execute(request);
    if (response is not null) return Results.Ok(response.Content);
    return Results.BadRequest();
});

app.Run();

